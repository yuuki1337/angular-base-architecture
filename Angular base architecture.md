# Angular base architecture

### Initialisation du projet

Pour commencer, je vous conseille de suivre l' excellent tutorial proposé par Tomas Trajan pour la création d'une architecture basique Angular ( https://medium.com/@tomastrajan/how-to-build-epic-angular-app-with-clean-architecture-91640ed1656 ). Nous expliquerons simplement ici la logique d'utilisation de l'architecture présentée. Remplacez simplement la ligne :

```
ng g application my-epic-app --prefix my-org --style scss --routing
```

par : 

```
ng new app-name --style scss --routing --strict
```

à la racine de votre dossier WorkSpace.



### Le tryptique "CoreModule, Lazy Features, SharedModule"

L'architecture proposée s'articule autour de 3 modules principaux, nous allons décrire ici leur utilité ainsi que la logique à adopter afin de tirer le meilleur parti de cette organisation du projet  :



#### Le CoreModule :

Le CoreModule comprend tous les "global singletons" ( services instanciés une fois et disponible à travers toute l' application ) nécessaires au fonctionnement de l' application et utilisés dans au moins 2 features. 

On retrouve ainsi  dans le dossier core les dossiers "guards", "services", "interceptors" à minima.



Il a aussi pour rôle de présenter le layout principal de l'application. Les seuls composants présents dans ce module sont donc ceux nécessaires à l'affichage du corps principal de notre application qui sera figé dans toutes les pages.

​																											 *workspace/app-name/src/app*

![CoreModule](./images/CoreModule.png)

Il importe strictement ce dont ont besoin ses components pour fonctionner ainsi que les modules Vendor nécessaires au fonctionnement de l'application. Il réexporte ensuite ses components afin que le app.module puisse en avoir connaissance :

​																														*core.module.ts*

![CoreModuleImportExport](./images/CoreModuleImportExport.png)

  Il est ensuite importé dans le app.module, ainsi épuré : 

​																														*app.module.ts*

![appModule](./images/appModule.png)

Pour résumer :

- Tous les services destinés à être utilisés dans plus d'une feature doivent se trouver dans le dossier "services" contenu dans le dossier "core".

  ```
  ng g s core/services/service-name
  ```

  pour générer un service global par exemple.

- Seul le layout principal disponible à travers toute l'application est contenu dans le CoreModule, ses composants doivent se trouver dans les déclarations ainsi que les exports du fichier core.module.ts.

  ```
  ng g c core/layout/main-layout
  ```

- Les guards et interceptors doivent aussi se trouve à la racine du dossier core, puisque ce sont aussi des "global singletons".

- Etant importé directement dans le AppModule, il n' est pas nécessaire de créer un module de routing pour le CoreModule.



#### Lazy Features :

Le dossier "features" ou "pages" comprend toutes les  features ( ^^ ) ajoutées à notre application ainsi que leurs composants associés.

Chaque feature présente dans ce dossier est créée avec son propre module ainsi que son module de routing. Elle contient seulement les composants non-génériques  indispensables à son bon fonctionnement. 

​																											 *workspace/app-name/src/app*

![features](./images/features.png)

Afin de créer sa route associée dans le app-routing.module.ts, à chaque ajout d' une nouvelle feature, utilisez la commande :

```
ng g m features/myfeatures --route my-features-route-name --module app.module.ts
```

Le module sera ainsi initialisé seulement à la navigation sur la route associée. 



​                                                                                                             *app-routing.module.ts*

![lazy](./images/lazy.png)



Sachez qu'il est possible de créer d'autres modules lazy-loadés à l' intérieur de chaque feature si cela vous semble nécessaire. Il faudra donc donner la bonne path à la commande --module.



Chaque feature peut aussi contenir un dossier "services" qui contiendra tout service spécifique à notre feature en question. Ces services ne doivent absolument pas être utilisés dans une autre feature.

A l'inverse des global singletons, ces services doivent supprimer leur propriété providedIn et s'injecter dans les providers du module correspondant :

​																							*dashboard/services/dashboard-specific.service.ts*

![providedIn](./images/providedIn.png)

![noProvider](./images/noProvider.png)

​																													*dashboard.module.ts*

![dashboard-specific](./images/dashboard-specific.png)



Pour résumer :

- Le dossier feature comprend les différentes pages/fonctionnalités de notre application découpées chacune en module.

- Chaque module contient uniquement les composants, services [...] qui lui sont absolument spécifique.

  ```
  ng g c features/myfeatures/feature-specific-component
  ```

  ```
  ng g s features/myfeatures/services/feature-specific-service
  ```

  

- Chaque module doit être lazy-loadé et contenir son propre module de routing, la route spécifié dans le app-routing.module définit la basepath de la navigation dans notre feature : 

  ```
  ng g m features/myfeatures --route my-features-route-name --module app.module.ts
  ```

#### Le SharedModule

Le SharedModule, généré dans le dossier "shared"  comprend, lui , toutes les déclarations génériques de l'application. On y retrouve :

-  Un dossier "components" qui contient tout composant réutilisé à travers au moins deux features.
-  Un dossier "interfaces" qui contient tous les modèles de données partagés à travers l' application.
-  Un dossier "helpers" qui contient toutes les fonctions helpers réutilisables.

En outre, il contiendra aussi toutes les directives ou custom pipes partagés dans des dossiers éponymes si besoin.

​																											  *workspace/app-name/src/app*

![shared](./images/shared.png)

Par exemple, un component "cards" réutilisable partout dans l' application qui aura pour simple fonction de recevoir des données et d'afficher une ou plusieurs cards sera généré via la commande :

```
ng g c shared/components/cards
```



Le SharedModule aura aussi pour rôle de reexporter les modules matérials et les modules vendors comme le "HttpClientModule" ou encore le "ReactiveFormsModule" souvent nécessaires dans plus d'un module de feature.

Tous les modules importés dans le SharedModule se retrouvent ainsi à la fois dans les imports et les exports du SharedModule tandis que les components se retrouvent à la fois dans les déclarations et les exports du SharedModule.

​																													*shared.module.ts*

![sharedModule](./images/sharedModule.png)

Le SharedModule est ensuite importé dans TOUS les modules lazy-loadés :

​																									    			 *dashboard.module.ts*

![lazyloadedShared](./images/lazyloadedShared.png)



Pour résumer :

- Le SharedModule exporte absolument tout ce qu' il importe et déclare.
- Le SharedModule est importé par TOUS les modules de feature lazy-loadés
- Il est inutile de créer un router associé au SharedModule
- Le SharedModule a pour responsabilité de réexporter à travers les features tous les modules Vendors nécessaires au fonctionnement de l'application : "HttpClientModule, ReactiveFormsModule, CommonModule, RouterModule [...] ainsi que les composants génériques"



Enjoy !!